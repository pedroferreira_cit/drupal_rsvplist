<?php

namespace Drupal\rsvplist\Plugin\Block;

use Drupal;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\rsvplist\Form\RSVPForm;

/**
 * Provides an RSVP list block
 * @Block(
 *   id = "rsvp_block",
 *   admin_label = @Translation("RSVP Block"),
 * )
 */
class RSVPBlock extends BlockBase {
  /**
   * (@inheritdoc)
   */
  public function build() {
    return Drupal::formBuilder()->getForm(RSVPForm::class);
  }
  
  /**
   * (@inheritdoc)
   */
  protected function blockAccess(AccountInterface $account) {
    $node = Drupal::routeMatch()->getParameter('node');
    if ($node && Drupal::service('rsvplist.enabler')->isEnabled($node)) {
      return AccessResult::allowedIfHasPermission($account, 'view rsvplist');
    }
    return AccessResult::forbidden();
  }
}
