<?php

namespace Drupal\rsvplist\Form;

use Drupal;
use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;

/**
 * Provides RSVP form
 *
 * @class RSVPForm
 */
class RSVPForm extends FormBase {
  /**
   * (@inheritdoc)
   */
  public function getFormId() {
    return 'rsvp_email_form';
  }

  /**
   * (@inheritdoc)
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $node = Drupal::routeMatch()->getParameter('node');
    return $form + [
      'email' => [
        '#title' => $this->t('Email Address'),
        '#type' => 'textfield',
        '#size' => 25,
        '#description' => $this->t("We'll send updates to the email address you provide."),
        '#required' => TRUE,
      ],
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('RSVP'),
      ],
      'nid' => [
        '#type' => 'hidden',
        '#value' => $node ? $node->nid->value : NULL,
      ]
    ];
  }

  /**
   * (@inheritdoc)
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $emailValue = $form_state->getValue('email');
    if (!Drupal::service('email.validator')->isValid($emailValue)) {
      $form_state->setErrorByName('email', $this->t(
        'The e-mail address %mail is not valid.',
        ['%mail' => $emailValue]
      ));
      return;
    }
    $node = Drupal::routeMatch()->getParameter('node');
    $existsCount = Database::getConnection()->select('rsvplist')
      ->condition('nid', $node->id())
      ->condition('mail', $emailValue)
      ->countQuery()
      ->execute()
      ->fetchField();
    if (intval($existsCount) > 0) {
      $form_state->setErrorByName('email', $this->t(
        'The address %mail is already subscribed to this list.',
        ['%mail' => $emailValue]
      ));
    }
  }

  /**
   * (@inheritdoc)
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $user = User::load(Drupal::currentUser()->id());
    Database::getConnection()->insert('rsvplist')
      ->fields([
        'mail' => $form_state->getValue('email'),
        'nid' => $form_state->getValue('nid'),
        'uid' => $user->id(),
        'created' => time(),
      ])
      ->execute();

    drupal_set_message($this->t('Thank for your RSVP, you are on the list for the event.'));
  }

}
