<?php

namespace Drupal\rsvplist\Form;

use Drupal;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides RSVP settings form
 *
 * @class RSVPForm
 */
class RSVPSettingsForm extends ConfigFormBase {
  /**
   * The form settings name
   *
   * @var string
   */
  private $name = 'rsvplist.settings';

  /**
   * (@inheritdoc)
   */
  public function getFormId() {
    return 'rsvplist_admin_settings';
  }

  /**
   * (@inheritdoc)
   */
  protected function getEditableConfigNames() {
    return [
        $this->name,
    ];
  }

  /**
   * (@inheritdoc)
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $node_types = node_type_get_names();
    $config = $this->config($this->name);
    $form += [
        'rsvplist_types' => [
            '#type' => 'checkboxes',
            '#title' => $this->t('The content types to enable RSVP collection for'),
            '#default_value' => $config->get('allowed_types'),
            '#options' => $node_types,
            '#description' => $this->t(
              'On the specified node types, as RSVP option wii be available and '
              . 'can be enabled while that node is being edited.'
            ),
        ],
        'array_filter' => [
            '#type' => 'value',
            '#value' => TRUE,
        ],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * (@inheritdoc)
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * (@inheritdoc)
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $allowed_types = array_filter($form_state->getValue('rsvplist_types'));
    sort($allowed_types);
    $this->config($this->name)
      ->set('allowed_types', $allowed_types)
      ->save();
    parent::submitForm($form, $form_state);
  }

}
