<?php

namespace Drupal\rsvplist\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;

/**
 * Report controller for RSVP module.
 */
class ReportController extends ControllerBase {

  /**
   * Creates the report page..
   *
   * @return array
   */
  public function report() {
    return [
      'message' => [
          '#markup' => $this->t('Below is a list of all events RSVP including'
          . ' username, e-mail address and the name of the event they will be attending.'),
      ],
      'table' => [
          '#type' => 'table',
          '#header' => [
              $this->t('Name'),
              $this->t('Event'),
              $this->t('E-mail'),
          ],
          '#rows' => array_map(function ($entry) {
            return array_map('Drupal\Component\Utility\SafeMarkup::checkPlain', $entry);
          }, $this->getEntries()),
      ],
      '#cache' => ['max-age' => 0],
    ];
  }

  /**
   * Gets all RSVP for all nodes.
   *
   * @return array
   */
  protected function getEntries() {
    $query = Database::getConnection()->select('rsvplist');
    $query->addField('rsvplist', 'mail');
    $query->addField('users_field_data', 'name', 'username');
    $query->addField('node_field_data', 'title');
    $query->join('users_field_data', NULL, 'rsvplist.uid = users_field_data.uid');
    $query->join('node_field_data', NULL, 'rsvplist.nid = node_field_data.nid');
    $entries = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
    return $entries;
  }

}
