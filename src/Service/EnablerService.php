<?php

namespace Drupal\rsvplist\Service;

use Drupal\node\Entity\Node;
use Drupal\Core\Database\Database;

/**
 * Service for managing RSVP enabled for nodes.
 */
class EnablerService {
  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private $connection;

  /**
   * RSVP enabled nodes table.
   *
   * @var string
   */
  private $table = 'rsvplist_enabled';

  /**
   * Create new service instance.
   */
  public function __construct() {
    $this->connection = Database::getConnection();
  }

  /**
   * Checks if individual node is RSVP enabled.
   *
   * @param \Drupal\node\Entity\Node $node
   * @return bool
   */
  public function isEnabled(Node $node) {
    if ($node->isNew()) {
      return FALSE;
    }
    $existsCount = $this->connection->select($this->table)
      ->condition('nid', $node->id())
      ->countQuery()
      ->execute()
      ->fetchField();
    
    return intval($existsCount) > 0;
  }

  /**
   * Sets a individual node to be RSVP enabled.
   *
   * @param \Drupal\node\Entity\Node $node
   * @return void
   */
  public function enableNode(Node $node) {
    if ($this->isEnabled($node)) {
      return;
    }
    $this->connection->insert($this->table)
      ->fields(['nid'], [$node->id()])
      ->execute();
  }

  /**
   * Sets a individual node to not be RSVP enabled.
   *
   * @param \Drupal\node\Entity\Node $node
   * @return void
   */
  public function disableNode(Node $node) {
    $this->connection->delete($this->table)
      ->condition('nid', $node->id())
      ->execute();
  }

}
